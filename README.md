# A proxy server that helps with node remote debugging #


  * Fetches UUID automatically - reuse the same url
  * Rewrites debugger host. Especially useful if node publishes wrong host e.g. from Docker container on Windows
  * Works with Node Inspector Manager

## Requirements ##

You can either run the proxy with Docker or starting index.js manually with node(don't forget npm install)

I use this mainly on Windows 10 with Git Bash and on plain Ubuntu. Should work on Mac right out of the bat too.
  
## Usage ##

	git clone https://peltoniemi@bitbucket.org/peltoniemi/node-inspect-proxy.git
	cd node-inspect-proxy
	
### Edit config.sh and run ###
	
	./build
	./start

### Or ###

	npm install
	node index.js
	
## Scripts ##

  * build - Builds the Docker image
  * start - Starts the proxy server. Reads config from _config.sh_ and by default attaches the container using ./attach
  * attach - Attaches the container using _docker attach_
  * stop - Stops and removes the container

## Accessing through browser ##

If you didn't change anything in config.sh, the following url should work:

_chrome-devtools://devtools/bundled/inspector.html?experiments=true&v8only=true&ws=127.0.0.1:9300/_

Connect to a different host by adding either **<host>:<port>**, **<host>** or **<port>** to the end of the url.

If you changed config.sh, starting the server will print your url using correct information.

The url doesn't change, so bookmark it for convenience.