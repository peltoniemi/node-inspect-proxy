var http = require('http'),
    httpProxy = require('http-proxy');

var env = process.env;
var options = {
    inspector: {
        port: env.PROXY_REMOTE_PORT ? env.PROXY_REMOTE_PORT : 9229,
        host: env.PROXY_REMOTE_HOST ? env.PROXY_REMOTE_HOST : '127.0.0.1'
    },
    expose: {
        port: env.PROXY_LISTEN_PORT ? env.PROXY_LISTEN_PORT : 9300,
        host: env.PROXY_LISTEN_HOST ? env.PROXY_LISTEN_HOST : '127.0.0.1',

        port_alias: env.PROXY_LISTEN_ALIAS,
    },
    appRoot: {
        local: '',
        remote: ''
    }
}
if (!options.expose.port_alias)
    options.expose.port_alias = options.expose.port;


var filterInspectorOptions = function(req) {
    var inspectorOptions = {
        host: options.inspector.host,
        port: options.inspector.port
    }
    var inspect = req.url.substring(1).split(':');

    if (inspect.length === 1 && inspect[0]) {
        if (inspect[0].match(/\D/)) {
            inspectorOptions.host = inspect[0];
        } else {
            inspectorOptions.port = inspect[0];
        }
    } else if (inspect.length === 2) {
        if (inspect[0])
            inspectorOptions.host = inspect[0];
        if (inspect[1])
            inspectorOptions.port = inspect[1];
    }

    return inspectorOptions;
}

var createProxy = function(proxyOptions) {

    console.log('Creating proxy',proxyOptions)
    var proxy = new httpProxy.createProxyServer({
        target: proxyOptions
    });
    proxy.on('error', function(error) {
        console.log('Proxy error: ', error);
        //that.checkSession().catch(that.close.bind(that));
    });
    proxy.on('connect', function() {
        console.log('Proxy connected');
    });
    proxy.on('close', function() {
        console.log('Closing proxy');
        //that.checkSession().catch(that.close.bind(that));
    });

    return proxy;
}



var getJsonData = function(inspectorOptions) {

    return new Promise(function(resolve, reject) {
        var requestOptions = {
            hostname: inspectorOptions.host,
            port: inspectorOptions.port,
            path: '/json',
            method: 'GET'
        };
        var clientRequest = http.request(requestOptions, function(clientResponse) {
            clientResponse.on('data', function(data) {
                data = data.toString();
                data = data.replace(/[0-9.]+:[0-9]+/g, options.expose.host + ':' + options.expose.port);
                data = data.replace(/(chrome-devtools:\/\/.*?)\/[0-9a-f\-]+"/, '$1"');
                // TODO Find out whether the url property in /json actually does something.
                //      Can't be bothered right now.
                //var url = data.match(/("file:\/\/)(.*?")/);
                //var newUrl = url[1] + url[2].replace(options.appRoot.remote, options.appRoot.local);
                //data = data.replace(url[0], newUrl);
                data = JSON.parse(data)[0];
                resolve(data);
            });

        });
        clientRequest.on('error', function(error) {
            console.error('Server connection error - cannot get JSON data: ' + error.code);
            resolve({});
        });
        clientRequest.end();
    });
}
var exposedServer = http.createServer(function(req, res) {
    var inspectorOptions = filterInspectorOptions(req);
    if (req.url == '/json') {
        getJsonData(true).then(function(data) {
            res.end(JSON.stringify([data]));
        })
    } else {
        var proxy = createProxy(inspectorOptions);
        if (req.url == '/') {
            getJsonData(true).then(function(data) {
                req.url = req.url + data.id;
                proxy.web(req, res);
            })
        } else {
            proxy.web(req, res);
        }

    }
});

//
// Listen to the `upgrade` event and proxy the
// WebSocket requests as well.
//
exposedServer.on('upgrade', function(req, socket, head) {
    console.info('Debugger connected');
    var inspectorOptions = filterInspectorOptions(req);
    var proxy = createProxy(inspectorOptions);
    req.url = '/';
    if (req.url == '/') {
        getJsonData(inspectorOptions).then(function(data) {
            var id = data.id ? data.id : '';
            req.url = req.url + id;
            proxy.ws(req, socket, head);
        })
    } else {
        proxy.ws(req, socket, head);
    }

});


exposedServer.listen(options.expose.port);

exposedServer.on('close', function() {
    console.log('Client disconnected');
});
exposedServer.on('error', function(error) {
    console.log('Client connection error', error.code);

});

exposedServer.on('connect', function(error) {
    console.log('Client connection error', error.code);
});
