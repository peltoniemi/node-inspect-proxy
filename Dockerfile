FROM node:7-alpine
RUN mkdir /app
WORKDIR /app
ADD ./package.json /app/package.json
RUN npm install
ADD ./index.js /app/index.js




EXPOSE 9300
ENTRYPOINT ["node", "index.js"]